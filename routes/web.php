<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// define global variable
view()->share('version', time());
view()->share('host_name', 'Sanura');

// Views
Route::get('/', 'HomeController@index')->name('home');

// Auth
Auth::routes();

// Admin group
Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{
    Route::get('/', function() {
        return redirect('/admin/dashboard');
    });
    Route::get('dashboard', 'admin\DashboardController@index')->name('dashboard');

    // User
    Route::get('user', 'admin\UserController@index')->name('user');
});
