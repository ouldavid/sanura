@extends('layouts.master')

@section('title', ucfirst(Route::currentRouteName()))

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('banner')
    <div id="banner">
        <div id="banner-slide">
            <div class="slide">
                <a href="#">
                    <div class="bg-slide d-none d-lg-block" style="background-image: url({{ asset('img/banner/banner-01.jpg') }})"></div>
                    <img class="img-fluid img-slide d-block d-lg-none" src="{{ asset('img/banner/banner-mobile-01.jpg') }}" alt="{{ $host_name }}" />
                </a>
            </div>
            <div class="slide">
                <a href="#">
                    <div class="bg-slide d-none d-lg-block" style="background-image: url({{ asset('img/banner/banner-01.jpg') }})"></div>
                    <img class="img-fluid img-slide d-block d-lg-none" src="{{ asset('img/banner/banner-mobile-01.jpg') }}" alt="{{ $host_name }}" />
                </a>
            </div>
        </div>
        <div class="over-banner d-none d-lg-block">
            <div class="frm-search d-none d-lg-block">
                <div class="form-group has-search position-relative mb-0">
                    <a href="#" class="form-control-feedback"><i class="fas fa-search"></i></a>
                    <input type="text" class="form-control" placeholder="Search">
                </div>
            </div>
            <div class="mt-3">
                <h4 class="text-uppercase">Fruits, Vegetables & Meat</h4>
                <button class="btn shop-now">Shop Now!</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="container-fluid p-0">
    <div class="container my-3">
        <div class="row">
            <div class="col-md-4 d-flex justify-content-center justify-content-md-start border-bt-dashed-md">
                <div class="float-left icon"><i class="fas fa-shipping-fast color-gray"></i></div>
                <div class="float-left">
                    <h4 class="color-gray text-uppercase mb-0">Free Shipping</h4>
                    <small class="color-gray text-uppercase">On Orders Over $99</small>
                </div>
            </div>
            <div class="col-md-4 d-flex justify-content-center border-bt-dashed-md">
                <div class="float-left icon"><i class="fas fa-percent color-gray"></i></div>
                <div class="float-left">
                    <h4 class="color-gray text-uppercase mb-0">Special Discounts</h4>
                    <small class="color-gray text-uppercase">Weekly Promotions</small>
                </div>
            </div>
            <div class="col-md-4 d-flex justify-content-center justify-content-md-end border-bt-dashed-md">
                <div class="float-left icon"><i class="fas fa-headset color-gray"></i></div>
                <div class="float-left">
                    <h4 class="color-gray text-uppercase mb-0">24/7 Customer Care</h4>
                    <small class="color-gray text-uppercase">Mon - Sun (8:00 - 20:00)</small>
                </div>
            </div>
        </div>
    </div>
    <hr class="d-none d-md-block">
</div>
<div class="container">
    <!-- home slide -->
    <div id="farm" class="mb-4">
        <div id="farm-slide">
            <div class="slide">
                <a href="#"><img class="img-fluid" src="{{ asset('img/home/vegetable.png') }}" alt="{{ $host_name }}" /></a>
            </div>
            <div class="slide">
                <a href="#"><img class="img-fluid" src="{{ asset('img/home/meat.png') }}" alt="{{ $host_name }}" /></a>
            </div>
            <div class="slide">
                <a href="#"><img class="img-fluid" src="{{ asset('img/home/fruit.png') }}" alt="{{ $host_name }}" /></a>
            </div>
            <div class="slide">
                <a href="#"><img class="img-fluid" src="{{ asset('img/home/meat.png') }}" alt="{{ $host_name }}" /></a>
            </div>
        </div>
    </div>
    <!-- hot deals -->
    <div id="hot-deal" class="mb-4">
        <h4 class="text-uppercase text-center mb-3">Hot Deals</h4>
        <div class="card-columns">
            <div class="card shadow-sm">
                <div class="card-header border-bottom-0 bg-transparent">
                    <div class="float-left">
                        <a href="#" class="pr-2"><i class="far fa-heart"></i></a>
                        <a href="#"><i class="fas fa-eye"></i></a>
                    </div>
                    <div class="float-right rating">
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Beef</h5>
                    <a href="#"><img class="card-img-top" src="{{ asset('img/products/beef.jpg') }}" alt="beef" /></a>
                </div>
                <div class="card-footer border-top-0 bg-transparent">
                    <p class="line-through">$3.00</p>
                    <div class="float-left">
                        <p class="font-weight-bold">$2.00</p>
                    </div>
                    <div class="float-right">
                        <button class="btn btn-slide slide-right">Add <i class="fas fa-shopping-basket"></i></button>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm">
                <div class="card-header border-bottom-0 bg-transparent">
                    <div class="float-left">
                        <a href="#" class="pr-2"><i class="far fa-heart"></i></a>
                        <a href="#"><i class="fas fa-eye"></i></a>
                    </div>
                    <div class="float-right rating">
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Broccoli</h5>
                    <a href="#"><img class="card-img-top" src="{{ asset('img/products/broccoli.jpg') }}" alt="broccoli" /></a>
                </div>
                <div class="card-footer border-top-0 bg-transparent">
                    <p class="line-through">$3.00</p>
                    <div class="float-left">
                        <p class="font-weight-bold">$2.00</p>
                    </div>
                    <div class="float-right">
                        <button class="btn btn-slide slide-right">Add <i class="fas fa-shopping-basket"></i></button>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm">
                <div class="card-header border-bottom-0 bg-transparent">
                    <div class="float-left">
                        <a href="#" class="pr-2"><i class="far fa-heart"></i></a>
                        <a href="#"><i class="fas fa-eye"></i></a>
                    </div>
                    <div class="float-right rating">
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Strawberries</h5>
                    <a href="#"><img class="card-img-top" src="{{ asset('img/products/strawberries.jpg') }}" alt="strawberries" /></a>
                </div>
                <div class="card-footer border-top-0 bg-transparent">
                    <p class="line-through">$3.00</p>
                    <div class="float-left">
                        <p class="font-weight-bold">$2.00</p>
                    </div>
                    <div class="float-right">
                        <button class="btn btn-slide slide-right">Add <i class="fas fa-shopping-basket"></i></button>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm">
                <div class="card-header border-bottom-0 bg-transparent">
                    <div class="float-left">
                        <a href="#" class="pr-2"><i class="far fa-heart"></i></a>
                        <a href="#"><i class="fas fa-eye"></i></a>
                    </div>
                    <div class="float-right rating">
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Chicken</h5>
                    <a href="#"><img class="card-img-top" src="{{ asset('img/products/chicken.jpg') }}" alt="chicken" /></a>
                </div>
                <div class="card-footer border-top-0 bg-transparent">
                    <p class="line-through">$3.00</p>
                    <div class="float-left">
                        <p class="font-weight-bold">$2.00</p>
                    </div>
                    <div class="float-right">
                        <button class="btn btn-slide slide-right">Add <i class="fas fa-shopping-basket"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="view-all mt-2">View all</a>
    </div><!-- #hot-deal -->
</div>
<div class="container-fluid p-0">
    <!-- mail subscribe -->
    <div id="mail-subscribe">
        <div class="d-flex align-items-center justify-content-center h-100">
            <form action="">
                <h4 class="text-uppercase text-center mb-3">Get The Latest News And Offers</h4>
                <div class="newsletter">
                    <input type="text" placeholder="Enter your mail">
                    <button type="submit">subscribe!</button>
                </div>
            </form>
        </div>

    </div><!-- #mail-subscribe -->
</div>

@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>
@endsection
