<nav class="navbar navbar-expand-lg navbar-dark sidebarNavigation p-0" data-sidebarClass="navbar-dark bg-green-dark">
    <div class="container-fluid container-lg">
        <a class="navbar-brand d-lg-block d-none" href="/"><img class="img-fluid" src="{{ asset('img/logo.png') }}" alt="{{ $host_name }}"></a>
        <a class="navbar-brand d-lg-none" href="/"><img class="img-fluid" src="{{ asset('img/logo-mobile.png') }}" alt="{{ $host_name }}"></a>
        <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse bg-green" id="navbarsExampleDefault">
            <button type="button" class="close d-lg-none" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="nav navbar-nav nav-flex-icons mr-auto">
                <li class="nav-item pl-0">
                    <a class="nav-link pl-0" href="/">
                        <i class="fas fa-home"></i>
                        <span data-hover="Home">Home</span>
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                        <span data-hover="Products">Products</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Link 1</a>
                        <a class="dropdown-item" href="#">Link 2</a>
                        <a class="dropdown-item" href="#">Link 3</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span data-hover="Blog">Blog</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span data-hover="Promotion">Promotion</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span data-hover="About us">About us</span>
                    </a>
                </li>
            </ul><!-- .navbar-nav -->
        </div><!-- #navbarsExampleDefault -->
        <div class="nav-right">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link position-relative" href="#">
                        <i class="fas fa-cart-plus extra"></i>
                        <span class="badge badge-pill badge-danger">1</span>
                    </a>
                </li>
                <li class="nav-item d-none d-lg-block">
                    <a class="nav-link btn btn-slide slide-right" href="#">
                        Join Now!
                    </a>
                </li>
                <li class="nav-item d-lg-none">
                    <a class="nav-link" href="#">
                        <i class="fas fa-user-plus"></i>
                    </a>
                </li>
                <li class="nav-item d-lg-none">
                    <a class="nav-link" href="#">
                        <i class="fas fa-search"></i>
                    </a>
                </li>
            </ul><!-- .nav-right -->
        </div>
    </div>
</nav>
