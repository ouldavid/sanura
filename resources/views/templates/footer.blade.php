<div class="container">
    <h4 class="text-capitalize text-center mb-3">Payment Method</h4>
    <ul class="online-card">
        <li><img src="{{ asset('img/cards/small-visa.png') }}" alt="Visa Card"/></li>
        <li><img src="{{ asset('img/cards/small-master.png') }}" alt="Master Card"/></li>
        <li><img src="{{ asset('img/cards/small-pipay.png') }}" alt="Pi Pay"/></li>
        <li><img src="{{ asset('img/cards/small-true.png') }}" alt="True Money"/></li>
        <li><img src="{{ asset('img/cards/small-wing.png') }}" alt="Wing"/></li>
    </ul>
</div>
<hr/>
<div class="container" id="list-info">
    <div class="row">
        <div class="col-md-4 d-flex justify-content-center justify-content-md-start mb-3">
            <ul>
                <li><h4 class="text-capitalize mb-3">Sanura Farm</h4></li>
                <li>
                    <i class="fas fa-caret-right"></i>
                    <a href="#" class="text-capitalize">About us</a>
                </li>
                <li>
                    <i class="fas fa-caret-right"></i>
                    <a href="#" class="text-capitalize">Payment method</a>
                </li>
                <li>
                    <i class="fas fa-caret-right"></i>
                    <a href="#" class="text-capitalize">How it work</a>
                </li>
                <li>
                    <i class="fas fa-caret-right"></i>
                    <a href="#" class="text-capitalize">Contact us</a>
                </li>
            </ul>
        </div>
        <div class="col-md-4 d-flex justify-content-center mb-3">
            <ul>
                <li><h4 class="text-capitalize mb-3">Quick Links</h4></li>
                <li>
                    <i class="fas fa-caret-right"></i>
                    <a href="#" class="text-capitalize">My account</a>
                </li>
                <li>
                    <i class="fas fa-caret-right"></i>
                    <a href="#" class="text-capitalize">Affiliate program</a>
                </li>
                <li>
                    <i class="fas fa-caret-right"></i>
                    <a href="#" class="text-capitalize">Terms & Conditions</a>
                </li>
                <li>
                    <i class="fas fa-caret-right"></i>
                    <a href="#" class="text-capitalize">Privacy</a>
                </li>
            </ul>
        </div>
        <div class="col-md-4 d-flex justify-content-center justify-content-md-end mb-3">
            <ul>
                <li><h4 class="text-capitalize mb-3">Social media</h4></li>
                <li>
                    <ul class="social-network social-circle">
                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="icoInstagram" title="Instagram"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#" class="icoYoutube" title="Youtube"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#" class="icoGoogle" title="Google +"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

</div>
